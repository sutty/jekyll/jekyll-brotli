# frozen_string_literal: true

module Jekyll
  module Brotli
    VERSION = "2.1.0"
  end
end
